import stringprep
import string
import re


class SimpleTokenizer():
    istr = "input string"
    marks = [';', '.', '!', ':', '،', '«',
             '»', '(', ')', '#', '$', '^', '*', '-']

    def __init__(self, istr):
        self.istr = istr.strip()

    def marks_replace(self):
        result = self.istr
        for mark in self.marks:
            # put space behind and after marks in context for splitting
            result = result.replace(mark, " {} ".format(mark))
        # remove semi-whitespace from context
        result = result.replace(u'\u200c', '')
        # remove extra whitespaces(more than 1)
        result = re.sub(' +', ' ', result)
        # remove white spcae from begin and end of context
        result = result.strip()
        self.istr = result
        return self

    def tokenize(self):
        self.marks_replace()
        result = self.istr.split(" ")
        return result
